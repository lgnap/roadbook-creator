<?php
namespace LGnap\Writer;

use Exception;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use PhpOffice\PhpWord\TemplateProcessor;

abstract class AbstractWriter
{
    public const FORMAT_PDF = 1;
    public const FORMAT_DOCX = 2;

    protected TemplateProcessor $templateProcessor;

    /**
     * @throws Exception
     */
    public static function getWriter(int $format, string $templateFile)
    {
        switch ($format) {
            default:
                throw new Exception('Format not supported');

            case self::FORMAT_PDF:
                return new PdfWriter($templateFile);
            case self::FORMAT_DOCX:
                return new DocxWriter($templateFile);
        }
    }

    /**
     * @throws CopyFileException
     * @throws CreateTemporaryFileException
     */
    public function __construct(string $templateFile)
    {
        $this->templateProcessor = new TemplateProcessor($templateFile);

    }

    public function setTitle(string $title): AbstractWriter
    {
        $this->templateProcessor->setValue('title', $title);

        return $this;
    }

    public function setParking(string $parking): AbstractWriter
    {
        $this->templateProcessor->setValue('parking', $parking);

        return $this;
    }

    /**
     * @param string[][] $waypoints
     */
    public function setWaypoints(array $waypoints)
    {
        $this->templateProcessor->cloneRowAndSetValues('gccode', $waypoints);
        $this->addImages($waypoints);
    }

    public function setImage(string $varName, string $path)
    {
        $this->templateProcessor->setImageValue($varName, $path);
    }
    /**
     * @return int[] key as string var name
     */
    public function getVars(): array
    {
        return $this->templateProcessor->getVariableCount();
    }

    /**
     * @param array $waypoints
     */
    private function addImages(array $waypoints): void
    {
        foreach ($waypoints as $i => $waypoint) {
            foreach ($waypoint as $varName => $value) {
                if (strpos($varName, 'image') === false) {
                    continue;
                }

                $this->setImage($varName . '#' . ($i + 1), $value);
            }
        }
    }

    public abstract function getExtension(): string;

    public abstract function generateDocument(string $fileName);

}