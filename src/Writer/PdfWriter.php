<?php
namespace LGnap\Writer;

use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Reader\ReaderInterface;
use PhpOffice\PhpWord\Settings;

class PdfWriter extends AbstractWriter
{
    private ReaderInterface $reader;

    public function __construct(string $templateFile)
    {
        parent::__construct($templateFile);

        Settings::setPdfRenderer(Settings::PDF_RENDERER_MPDF, '/tmp');

        $this->reader = IOFactory::createReader();
    }

    /**
     * @throws Exception
     */
    public function generateDocument(string $fileName)
    {
        $fileGeneratedPath = $this->templateProcessor->save();
        $phpWord = $this->reader->load($fileGeneratedPath);

        $objWriter = IOFactory::createWriter($phpWord, 'PDF');

        $objWriter->save($fileName);
    }

    public function getExtension(): string
    {
        return 'pdf';
    }
}
