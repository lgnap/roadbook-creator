<?php
namespace LGnap\Writer;

use PhpOffice\PhpWord\Exception\Exception;

class DocxWriter extends AbstractWriter
{
    public function generateDocument(string $fileName)
    {
        $this->templateProcessor->saveAs($fileName);
    }

    public function getExtension(): string
    {
        return 'docx';
    }
}
