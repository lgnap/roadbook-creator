.PHONY=setup-env docker-rebuild

setup-env: composer.phar vendor

composer.phar:
	wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet

vendor:
	docker-compose run php ./composer.phar install

docker-rebuild:
	docker-compose build