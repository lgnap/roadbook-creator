<?php
require 'vendor/autoload.php';

use LGnap\Writer\AbstractWriter;

$writer = AbstractWriter::getWriter(AbstractWriter::FORMAT_DOCX, __DIR__ . '/templates/basic.docx');

$writer->setWaypoints([['search-image-pattern' => 'images/type_mystery.png']]);

if (!file_exists(__DIR__ . '/tmp')) {
    mkdir(__DIR__ . '/tmp');
}

var_dump($writer->getVars());

$writer->generateDocument(__DIR__ . '/tmp/generated.' . $writer->getExtension());
