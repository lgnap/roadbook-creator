FROM php:7.4-cli

RUN apt-get update && apt-get install -y \
    git \
    zip unzip \
    libzip-dev

RUN  docker-php-ext-install zip

RUN apt-get update && apt-get install -y \
    libpng-dev zlib1g-dev

RUN docker-php-ext-install gd